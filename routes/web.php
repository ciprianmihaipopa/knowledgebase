<?php


Route::get('/', 'PageController@index');

Route::get('/article/{article}', 'PageController@article');

Route::get('/search/', 'PageController@search');


Route::get('/admin/', 'ArticleController@index')->middleware('auth');

Auth::routes();


Route::get('/admin/users', 'UserController@index')->middleware('auth');

Route::get('/admin/users/create', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware('auth');

Route::post('/admin/users/create', 'Auth\RegisterController@register');

Route::get('/admin/users', 'UserController@index')->middleware('auth');


Route::resource('/admin/articles', 'ArticleController')->middleware('auth');

Route::post('/admin/articles/add-step', 'ArticleController@addStep');
