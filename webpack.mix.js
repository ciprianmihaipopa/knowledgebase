const mix = require('laravel-mix');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .extract(['vue', 'bootstrap', 'jquery', 'chart.js', 'moment', 'datatables.net-bs4', 'datatables.net-responsive-bs4', 'bootstrap-colorpicker', 'tempusdominus-bootstrap-4'])
    .scripts(
        [
            'resources/js/sb-admin-2.js',
            'resources/js/datatables-demo.js'
        ],
        'public/js/theme.js'
    );

mix.js('resources/knowledgebase/js/app.js', 'public/js')
    .sass('resources/knowledgebase/sass/app.scss', 'public/knowledgebase/css')


mix.browserSync({port: 80, proxy: 'knowledgebase'});
