<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    protected $fillable = [
        'title'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function steps()
    {

        return $this->hasMany('App\Step');

    }

    /**
     * @return mixed
     */
    public function deleteSteps()
    {

        return $this->steps()->delete();

    }

    /**
     * @param $content_fields
     *
     */
    public function processSteps($content_fields)
    {

        foreach ($content_fields as $key => $content) {

            $dom = new \DomDocument();

            $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

            $images = $dom->getElementsByTagName('img');

            foreach ($images as $k => $img) {

                $data = $img->getAttribute('src');

                $data = base64_decode($data);

                $image_name = "/upload/" . time() . $k . '.png';

                $path = public_path() . $image_name;

                file_put_contents($path, $data);

                $img->removeAttribute('src');

                $img->setAttribute('src', $image_name);

            }

            Step::create(
                ['content' => $content, 'article_id' => $this->id]
            );

        }

        return;
    }

}
