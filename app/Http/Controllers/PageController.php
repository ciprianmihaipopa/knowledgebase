<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class PageController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display the index page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $articles = DB::table('articles')->select('*')->orderBy('updated_at', 'desc')->take(10)->get();

        return view('knowledgebase_home', compact('articles'));

    }

    /**
     * Display the article page
     *
     * @param Article $article
     * @return \Illuminate\Http\Response
     */
    public function article(Article $article)
    {

        $steps = $article->steps;

        return view('articles.front', compact('article', 'steps'));

    }

    /**
     * Display the search page
     *
     * @param Request $request
     * @return void
     */
    public function search(Request $request)
    {

        $query = $request->input('q');

        $articles = Article::where('title', 'LIKE', '%' . $query . '%')
            ->orWhereHas(
                'steps', function ($eloquent) use ($query) {
                $eloquent->where('content', 'LIKE', '%' . $query . '%');
            })->get();

        $search = true;

        return view('knowledgebase_home', compact('articles', 'search', 'query'));

    }
}
