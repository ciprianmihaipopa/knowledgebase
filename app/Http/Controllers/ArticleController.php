<?php

namespace App\Http\Controllers;

use App\Article;
use App\Step;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $articles = Article::all();

        return view('articles.index', compact('articles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'content' => 'required',
                'content.*' => 'required',
                'title' => 'required'
            ]
        );

        $steps = [];

        $content_fields = $request->input('content');

        $article = Article::create(['title' => $request->input('title')]);

        $article->processSteps($content_fields);

        return redirect()->route('articles.edit', ['article' => $article->id]);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Article $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $steps = $article->steps;

        return view('articles.edit', compact('article', 'steps'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Article $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {

        $this->validate(
            $request,
            [
                'content' => 'required',
                'content.*' => 'required',
                'title' => 'required'
            ]
        );

        $content_fields = $request->input('content');

        $article->update(request(['title']));

        $article->deleteSteps();

        $article->processSteps($content_fields);

        return redirect()->route('articles.edit', ['article' => $article->id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Article $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()->route('articles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Article $article
     */
    public function addStep(Article $article)
    {

        $id = uniqid();

        return response()->json(
            ['html' => view(
                'parts.step_form_input',
                compact('id')
            )->render(), 'id' => $id]
        );

    }
}
