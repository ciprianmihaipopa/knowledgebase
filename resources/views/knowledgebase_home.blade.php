@extends('layouts.knowledgebase')

@section('content')
    <!-- Masthead -->
    <header class="masthead text-white text-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <h1 class="mb-5">Got questions? We've got the answers!</h1>
                </div>
                <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                    <form action="/search">
                        <div class="form-row">
                            <div class="col-12 col-md-9 mb-2 mb-md-0">
                                <input name="q" type="text" class="form-control form-control-lg"
                                       placeholder="How can we help you?" value="@isset($query) {{$query}} @endisset">
                            </div>
                            <div class="col-12 col-md-3">
                                <button type="submit" class="btn btn-block btn-lg btn-primary">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>

    <section class="features-icons bg-light text-center">
        @if(isset($search))
            <div class="container"><div class="row"><h2 class="mb-4"> Results </h2></div></div>
        @else
            <h2 class="mb-4">Latest articles</h2>
        @endif

        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <ul class="list-group">
                        @foreach($articles as $article)
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-md-8">
                                        <a href="/article/{{$article->id}}">{{$article->title}}</a>
                                    </div>
                                    <div class="col-md-4">
                                        {{Carbon::parse($article->updated_at)->diffForHumans()}}
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection
