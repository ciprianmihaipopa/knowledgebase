@extends('layouts.layout')

@section('title', 'Edit Article')
@section('description', 'Knowledgebase Pro Add a Article')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @component('components.card')

                @slot('title')
                    Edit "{{$article->title}}" <a class="right text-red pl-3 " href="/article/{{$article->id}}">View on site</a>
                @endslot

                <form method="POST" action="/admin/articles/{{$article->id}}" class="summernote-form">

                    @csrf
                    @method('PATCH')

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Article title</label>
                                <input type="text" class="form-control" id="title" name="title"
                                       placeholder="Enter the article title" required="required"
                                       value="{{$article->title}}">
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>

                    @isset($steps)
                        <div class="steps-container">
                            @foreach($steps as $key => $step)
                                @include('parts.step_form_input', ['id' => $step->id, 'key' => $key, 'value' => $step->content])
                            @endforeach
                        </div>
                    @endisset

                    @include('parts.errors')

                    <div class="alert alert-danger alert-dismissible fade" id="note_empty_error" role="alert">
                        <strong></strong> You should fill all the fields above.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <button type="button" class="btn btn-dark" onclick="addStep('{{  csrf_token() }}');">Add step
                    </button>
                    <button type="submit" class="btn btn-primary">Submit</button>

                </form>

            @endcomponent
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            @component('components.card')

                @slot('title')
                    Delete '{{$article->title}}'
                @endslot

                <form method="POST" action="/admin/articles/{{$article->id}}">

                    @csrf
                    @method('DELETE')

                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <button type="submit" class="btn btn-danger">Delete</button>

                </form>

            @endcomponent
        </div>
    </div>
@endsection
