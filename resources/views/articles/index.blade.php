@extends('layouts.layout')

@section('title', 'Articles List')
@section('description', 'Knowledgebase Pro Article List')

@section('content')
    @component('components.card')

        @slot('title')
            A list of all recorded articles
        @endslot
        <div class="table-responsive">
            <table class="table table-striped table-bordered responsive table-sm" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Title</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th>Actions</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach($articles as $article)
                    <tr>
                        <td>{{$article->title}}</td>
                        <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $article->created_at)->format('Y/m/d H:i')}}</td>
                        <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $article->updated_at)->format('Y/m/d H:i')}}</td>
                        <td>
                            <a href="{{route('articles.edit', ['article' => $article->id])}}" class="btn">
                                <i class="fas fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endcomponent
@endsection
