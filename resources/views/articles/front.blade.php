@extends('layouts.knowledgebase')

@section('content')
    <header class="masthead text-white article-pad">
        <div class="overlay"></div>
        <div class="container">
            <div class="row text-center">
                <div class="col-xl-9 mx-auto">
                    <h1 class="mb-5">{{$article->title}}</h1>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$article->title}}</li>
                    @if(Auth::check())
                        <a class="text-red pl-3" href="{{ route('articles.edit', ['article' => $article->id]) }}">Edit</a>
                    @endif
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        @foreach($steps as $key => $step)
            <div class="mb-3">
                <div class="step-heading">
                    @if(count($steps) > 1 && $loop->last)
                        <h3>Conclusion</h3>
                    @else
                        <h3>Step #{{$key + 1}}</h3>
                    @endif
                </div>
                <div class="step-content">
                    {!! $step->content !!}
                </div>
            </div>
        @endforeach
    </div>
@endsection
