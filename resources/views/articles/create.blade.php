@extends('layouts.layout')

@section('title', 'Create Articles')
@section('description', 'EasyArticles Pro Add a Article')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @component('components.card')

                @slot('title')
                    Create a new article
                @endslot

                <form method="POST" action="/admin/articles" class="summernote-form">

                    @csrf

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Article title</label>
                                <input type="text" class="form-control" id="title" name="title"
                                       placeholder="Enter the article title" required="required">
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>

                    <div class="steps-container">

                    </div>


                    @include('parts.errors')

                    <div class="alert alert-danger alert-dismissible fade" id="note_empty_error" role="alert">
                        <strong></strong> You should fill all the fields above.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <button type="button" class="btn btn-dark" onclick="addStep('{{  csrf_token() }}');">Add step</button>
                    <button type="submit" class="btn btn-primary">Submit</button>

                </form>

            @endcomponent
        </div>
    </div>
@endsection
