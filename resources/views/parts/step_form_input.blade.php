<div class="step step_{{ $id }} form-group">
    <div class="row">
        <div class="col-md-8">
            <label class="step-toggle"
                   onclick="jQuery(this).find('i').toggleIcon('fas fa-chevron-down fa-xs', 'fas fa-chevron-up fa-xs')">
                <a role="button" data-toggle="collapse" data-target="#step_{{ $id }}" aria-expanded="false"
                   aria-controls="step_{{ $id }}">
                    Step #<span class="step-number">@isset($key) {{ $key + 1 }} @endisset</span>
                    <i class="fas fa-chevron-up fa-xs"></i>
                </a>
            </label>
        </div>
        <div class="col-md-4">
            <button type="button" onclick="removeStep('step_{{ $id }}');" class="btn btn-danger right delete-btn"><i class="fa fa-trash"></i></button>
        </div>
    </div>


    <div class="collapse show" id="step_{{ $id }}">
        <textarea id="summernote_{{ $id }}" name="content[]" class="summernote"
        >@isset($value) {{ $value }} @endisset</textarea>
    </div>

</div>
