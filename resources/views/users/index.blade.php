@extends('layouts.layout')

@section('title', 'Users List')
@section('description', 'Knowledgebase Pro Users List')

@section('content')
    @component('components.card')

        @slot('title')
          A list of all recorded Questions
        @endslot
	        <div class="table-responsive">
                <table class="table table-striped table-bordered responsive table-sm" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Username</th>
                      <th>E-mail address</th>
                      <th>Role</th>
                      <th>Date added</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Name</th>
                      <th>Username</th>
                      <th>E-mail address</th>
                      <th>Date added</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  	@foreach($users as $user)
                  		<tr>
                  			<td>{{$user->name}}</td>

                  			<td>{{$user->username}}</td>

                        <td>{{$user->email}}</td>

                        <td>{{$user->role}}</td>

                  			<td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('Y/m/d H:i')}}</td>
                  			<!--td>
                  				<a href="/users/{{$user->id}}/edit" class="btn">
				                    <i class="fas fa-edit"></i>
				                  </a>
				                </td-->
                  		</tr>
                  	@endforeach
                  </tbody>
                </table>
            </div>
    @endcomponent
@endsection
