
# Knowledgebase PRO


## Server requirements 
###Knowledgebase PRO is built on Laravel 5.8, which requires the following:

- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension
 
- Node.js with NPM
- Composer
 

## Installation

 1. Run `composer install` to install the Composer dependencies.
 2.  Run `npm install` to install NPM dependencies. 
 3. Copy the example environment configuration file
		`cp .env.example .env`
 4. Run `php artisan key:generate` to generate an app key. It will be stored in the .env file. 
 5. Create an empty database for the application.
  > **Note:** MySQL 5.7.19 was used in development, but other database systems should be fine.
 6. In the .env file, add database information to allow Laravel to connect to the database
	```
		  DB_CONNECTION=mysql
		  DB_HOST=127.0.0.1     
		  DB_PORT=3306     
		  DB_DATABASE=homestead     
		  DB_USERNAME=homestead      
		  DB_PASSWORD=secret
	```     
 7. Migrate the database `php artisan migrate`
 8. Run `php artisan db:seed` to populate the database with dummy data. [Faker](https://github.com/fzaninotto/Faker) is used to add:
  * an admin user with the username `admin` and password `secret`

If only the admin user is wanted and no dummy data, run `php
php artisan db:seed --class=AdminSeeder` instead.
Start a Laravel development server by running `php artisan serve`. 
      
## License
[MIT](https://choosealicense.com/licenses/mit/)

