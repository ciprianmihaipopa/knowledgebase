<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
  
        for($i=0;$i<10;$i++){
        	DB::table('users')->insert([
	            'name' => $faker->name,
	            'email' => $faker->email,
	            'password' => bcrypt('secret'),
	            'username' => $faker->userName,
	            'role' => 'employee',
	            'created_at' => Carbon::now()->toDateTimeString(),
	            'updated_at' => Carbon::now()->toDateTimeString()
        	]);
        }
    }
}
