(function ($) {
    "use strict"; // Start of use strict

    // Toggle the side navigation
    $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
        if ($(".sidebar").hasClass("toggled")) {
            $('.sidebar .collapse').collapse('hide');
        }
    });

    // Close any open menu accordions when window is resized below 768px
    $(window).resize(function () {
        if ($(window).width() < 768) {
            $('.sidebar .collapse').collapse('hide');
        }
        ;
    });

    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
        if ($(window).width() > 768) {
            var e0 = e.originalEvent,
                delta = e0.wheelDelta || -e0.detail;
            this.scrollTop += (delta < 0 ? 1 : -1) * 30;
            e.preventDefault();
        }
    });

    // Scroll to top button appear
    $(document).on('scroll', function () {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function (e) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top)
        }, 1000, 'easeInOutExpo');
        e.preventDefault();
    });

    $(document).ready(function () {
        $('.summernote').summernote({
            minHeight: 300,
            codeviewFilter: true,
            codeviewIframeFilter: true
        });
        $('.summernote-form').on('submit', function (e) {
            if(jQuery('.summernote').summernote('isEmpty')) {
                e.preventDefault();
                $('#note_empty_error').addClass('show');
            }
        });
    });

    $.fn.extend(
        {
            toggleIcon: function (a, b) {
                return this.attr('class', this.attr('class') == b ? a : b);
            }
        }
    );


})(jQuery); // End of use strict

function addStep(csrf_token) {

    jQuery.ajax(
        {
            type: 'POST',
            url: '/admin/articles/add-step',
            data: {"_token": csrf_token},
            cache: false,
            success: function (response) {
                if (response.id) {
                    jQuery('.steps-container').append(response.html).ready(
                        function () {
                            jQuery("#summernote_" + response.id).summernote({minHeight: 300}).focus();
                            var count = jQuery('.step').length;
                            jQuery(".step_" + response.id + " .step-number").html(count);
                            document.querySelector(".step_" + response.id).scrollIntoView({ behavior: 'smooth' })
                        }
                    );
                }
            }
        }
    );

    return true;

}

function removeStep(id) {

    jQuery('.' + id).remove();

    jQuery('.step').each(
        function (index) {
             jQuery(this).find('.step-number').html(parseInt(index) + 1);
        }
    );

}


// Call the dataTables jQuery plugin
jQuery(document).ready(function() {
  jQuery('#dataTable').DataTable();
});
